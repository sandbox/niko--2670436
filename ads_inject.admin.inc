<?php
/**
 * @file
 * Module settings.
 */

/**
 * Implements hook_form().
 */
function ads_inject_form($node, &$form_state, $zone_id) {
  $openx_zones = variable_get('openx_zones');
  $name = 'ads_inject_zone-' . $zone_id;
  $node_types = node_type_get_names();
  $serialized_settings = variable_get($name);
  $settings = unserialize($serialized_settings);
  $list_themes = list_themes();
  $enabled_themes = array();

  // Get enabled themes list.
  foreach ($list_themes as $theme_key => $item_theme) {
    if ((bool) $item_theme->status) {
      $enabled_themes[$theme_key] = $item_theme->info['name'];
    }
  }
  $form['block_name'] = array(
    '#type' => 'item',
    '#title' => t('Block name'),
    '#title_display' => 'invisible',
    '#markup' => $openx_zones[$zone_id]['name'],
    '#field_prefix' => '<h2>',
    '#field_suffix' => '</h2>',
  );
  $form['status'] = array(
    '#title' => t('Enable, this action'),
    '#type' => 'checkbox',
    '#default_value' => isset($settings['status']) ? $settings['status'] : 0,
    '#options' => array(
      0 => t('Disable'),
      1 => t('Enable'),
    ),
  );
  $form['conditionals'] = array(
    '#title' => t('The condition'),
    '#type' => 'fieldset',
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
    '#states' => array(
      'visible' => array(
        'input[name="status"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['content_type'] = array(
    '#type' => 'select',
    '#title' => t('Available Content Type(s)'),
    '#description' => t('Which content type would you like this region to be
      injected in the middle of?'),
    '#options' => $node_types,
    '#default_value' => $settings['content_type'],
    '#multiple' => TRUE,
    '#states' => array(
      'visible' => array(
        'input[name="status"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['conditionals']['paragraph_operator'] = array(
    '#type' => 'select',
    '#options' => array(
      '<' => t('less than'),
      '=' => t('exactly'),
      '>' => t('more than'),
    ),
    '#default_value' => $settings['paragraph_operator'],
    '#multiple' => FALSE,
    '#prefix' => t('The destination needs to have'),
  );
  $form['conditionals']['paragraph_number'] = array(
    '#type' => 'textfield',
    '#maxlength' => 3,
    '#default_value' => $settings['paragraph_number'],
    '#size' => 4,
    '#suffix' => t('paragraphs in order to fire the offset action below.'),
  );
  $form['theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#options' => $enabled_themes,
    '#default_value' => $settings['theme'],
    '#description' => t('Select theme to which to apply the settings.'),
    '#states' => array(
      'visible' => array(
        'input[name="status"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['float'] = array(
    '#title' => t('Float left'),
    '#type' => 'checkbox',
    '#default_value' => isset($settings['float']) ? $settings['float'] : 0,
    '#options' => array(
      0 => t('Disable'),
      1 => t('Enable'),
    ),
    '#states' => array(
      'visible' => array(
        'input[name="status"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['action'] = array(
    '#description'  => t('The action to take if the condition above is met.'),
    '#title'        => t('THE ACTION'),
    '#type'         => 'fieldset',
    '#prefix' => '<div id="embed-actions-wrapper">',
    '#suffix' => '</div>',
    '#states' => array(
      'visible' => array(
        'input[name="status"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['action']['paragraph_offset'] = array(
    '#description'  => t('Please specify a positive or negative number to offset the injection (e.g. 1 to move down by one paragraph / -1 to move up by one paragraph)'),
    '#title'        => t('Paragraph offset'),
    '#default_value' => $settings['paragraph_offset'],
    '#type'         => 'textfield',
  );
  $form['action']['top_limit'] = array(
    '#description'  => t('Please specify a positive number of top limit'),
    '#title'        => t('Top limit'),
    '#default_value' => isset($settings['top_limit']) ? $settings['top_limit'] : 7,
    '#type'         => 'textfield',
  );
  $form['settings']['zone_id'] = array(
    '#type' => 'value',
    '#value' => $zone_id,
  );

  // Per-path visibility.
  $form['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'visibility',
    '#states' => array(
      'visible' => array(
        'input[name="status"]' => array('checked' => TRUE),
      ),
    ),
  );
  $options = array(
    0 => t('All pages except those listed'),
    1 => t('Only the listed pages'),
  );
  $form['path']['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show block on specific pages'),
    '#options' => $options,
    '#default_value' => isset($settings['visibility']) ? $settings['visibility'] : '',
  );
  $form['path']['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#title_display' => 'invisible',
    '#default_value' => isset($settings['pages']) ? $settings['pages'] : '',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function ads_inject_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['paragraph_offset'])) {
    form_set_error('paragraph_offset', t('Enter an integer value.'));
  }
  if (!is_numeric($form_state['values']['paragraph_number'])) {
    form_set_error('paragraph_number', t('Enter an integer value.'));
  }
  if (!is_numeric($form_state['values']['top_limit']) || $form_state['values']['top_limit'] <= 0) {
    form_set_error('top_limit', t('Enter an integer value more then 0.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function ads_inject_form_submit($form, &$form_state) {
  $name = 'ads_inject_zone-' . $form_state['values']['zone_id'];
  $settings = array(
    'paragraph_operator' => $form_state['values']['paragraph_operator'],
    'paragraph_number' => $form_state['values']['paragraph_number'],
    'content_type' => $form_state['values']['content_type'],
    'theme' => $form_state['values']['theme'],
    'float' => $form_state['values']['float'],
    'status' => $form_state['values']['status'],
    'paragraph_offset' => $form_state['values']['paragraph_offset'],
    'top_limit' => $form_state['values']['top_limit'],
    'visibility' => $form_state['values']['visibility'],
    'pages' => $form_state['values']['pages'],
  );
  $serialized_settings = serialize($settings);
  variable_set($name, $serialized_settings);
  drupal_set_message(t('Action successful added.'));
}
