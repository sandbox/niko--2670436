<?php
/**
 * @file
 * Contains the simple display renderer.
 */

/**
 * The standard display renderer renders a display normally, except each pane
 * is already rendered content, rather than a pane containing CTools content
 * to be rendered. Styles are not supported.
 */
class ads_inject_renderer_standard extends panels_renderer_standard {
  function render_region($region_id, $panes) {
    $regions = array('center', 'content');
    $output = parent::render_region($region_id, $panes);
    if (in_array($region_id, $regions)) {
      $zones_settings = ads_inject_get_zones();
      if (empty($zones_settings)) {
        return $output;
      }
      $this->injectAds($output, $zones_settings);
    }
    return $output;
  }

  function injectAds(&$output, $zones_settings) {
    $dom = new DOMDocument('1.0', 'UTF-8');
    libxml_use_internal_errors(TRUE);
    @$dom->loadHTML(mb_convert_encoding('<body>' . $output . '</body>', 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $dom->encoding = 'UTF-8';
    $content_xpath = new DOMXPath($dom);
    $paragraphs = $content_xpath->evaluate('//div[@class="field-item even"]/p');
    $elements_count = $paragraphs->length;
    $node_type = $this->display->context['panelizer']->data->type;
    _ads_inject_filter_zones_by_type($zones_settings, $node_type);

    global $theme;
    foreach ($zones_settings as $id => $setting) {
      if ($zones_settings[$id]['theme'] == $theme && $zones_settings[$id]['status']) {
        switch ($setting['paragraph_operator']) {
          case '=':
            if ($setting['paragraph_number'] == $elements_count) {
              ads_inject_build_body($output, $setting, $id);
            }
            break;

          case '>':
            if ($elements_count > $setting['paragraph_number']) {
              ads_inject_build_body($output, $setting, $id);
            }
            break;

          case '<':
            if ($elements_count < $setting['paragraph_number']) {
              ads_inject_build_body($output, $setting, $id);
            }
            break;
        }
      }
    }
    $output = html_entity_decode($output);
  }
}
